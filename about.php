<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>About: Characters Database</title>
<meta name="description"
     content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
</head>

<body>
<div id="page-container">
<?php
include_once('inc/header.html');
?>

<div id="main-content">
<h1>About the Fictional Characters Database</h1>
<p>The Fictional Characters Database(FCDB) is a project to produce a database
of fictional characters, and their appearances in differint media, as well
as who played them. The goal is collect a list of all fictional characters
in all forms of media, and cross-reference them with their appearances in
media. Say you wanted to know what tv shows have an appearance of Doctor Doom
in it, this project would allow you to do that, it would also tell you which
episodes that character appears in.</p>


</div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
</div><!-- page-container -->
</body>
</html>

