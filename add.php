<!DOCTYPE html>
<html>
<head>
<title>Add: Characters Database</title>
<meta name="description"
     content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
</head>

<body>
<div id="page-container">
<?php
include_once('inc/header.html');
include_once('inc/db.php');
include_once('inc/const.php');
?>

<div id="main-content">
<h1>Record Contribution Status</h1>
<?php
switch($_POST['add_t']){
case ADDCHRAPP:
    $sql = 'INSERT INTO CHRAPPS(CHARID,MEDIAID,EPISODEID,NOTES)VALUES(:CHR,:MED,:EPI,:NOT);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':CHR',$_POST['chr'],PDO::PARAM_INT);
    $sth->bindParam(':MED',$_POST['med'],PDO::PARAM_INT);
    $sth->bindParam(':EPI',$_POST['epi'],PDO::PARAM_INT);
    $sth->bindParam(':NOT',$_POST['notes'],PDO::PARAM_STR);
    $stat = $sth->execute();
    break;
case ADDACTAPP:
    $sql = 'INSERT INTO ACTAPPS(ACTID,CHRID,MEDIAID)VALUES(:ACT,:CHR,:MED);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':ACT',$_POST['actor'],PDO::PARAM_INT);
    $sth->bindParam(':CHR',$_POST['chr'],PDO::PARAM_INT);
    $sth->bindParam(':MED',$_POST['med'],PDO::PARAM_INT);
    $stat = $sth->execute();
    break;
case ADDACT:
    $sql = 'INSERT INTO ACTORS(NAME,WIKILINK)VALUES(:NAM,:WIKI);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':NAM',$_POST['name'],PDO::PARAM_STR);
    $sth->bindParam(':WIKI',$_POST['wiki'],PDO::PARAM_STR);
    $stat = $sth->execute();
    break;
case ADDCHR:
    $sql = 'INSERT INTO CHARACTERS(UNIVID,NAME,DESCR)VALUES(:uni,:nam,:desc);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':uni',$_POST['uni'],PDO::PARAM_INT);
    $sth->bindParam(':nam',$_POST['name'],PDO::PARAM_STR);
    $sth->bindParam(':desc',$_POST['desc'],PDO::PARAM_STR);
    $stat = $sth->execute();
    break;
case ADDEPI:
    $sql = 'INSERT INTO EPISODES(SEASON,EPISODE,MEDIAID,NAME,AIRDATE,SYNOPSIS)VALUES(:sea,:epi,:med,:nam,to_date(:air, \'MM/DD/YYYY\'),:desc);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':sea',$_POST['season'],PDO::PARAM_INT);
    $sth->bindParam(':epi',$_POST['epinum'],PDO::PARAM_INT);
    $sth->bindParam(':med',$_POST['med'],PDO::PARAM_INT);
    $sth->bindParam(':nam',$_POST['name'],PDO::PARAM_STR);
    $sth->bindParam(':air',$_POST['airdate'],PDO::PARAM_STR);
    $sth->bindParam(':desc',$_POST['desc'],PDO::PARAM_STR);
    $stat = $sth->execute();
    break;
case ADDMED:
    $sql = 'INSERT INTO MEDIAS(NAME,WIKILINK,MEDIA,YEARAIRED,DESCR)VALUES(:nam,:wiki,:med,:air,:desc)';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':nam',$_POST['name'],PDO::PARAM_STR);
    $sth->bindParam(':wiki',$_POST['wiki'],PDO::PARAM_STR);
    $sth->bindParam(':med',$_POST['media_t'],PDO::PARAM_INT);
    $sth->bindParam(':air',$_POST['year'],PDO::PARAM_INT);
    $sth->bindParam(':desc',$_POST['desc'],PDO::PARAM_INT);
    $stat = $sth->execute();
    break;
case ADDUNI:
    $sql = 'INSERT INTO UNIVERSES(NAME,DESCR)VALUES(:nam,:desc);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':nam',$_POST['name'],PDO::PARAM_STR);
    $sth->bindParam(':desc',$_POST['desc'],PDO::PARAM_STR);
    $stat = $sth->execute();
    break;
case ADDCHRALIAS:
    $sql = 'INSERT INTO CHRALIASES(CHARID,NAME) VALUES(:chr,:nam);';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':chr',$_POST['chr'],PDO::PARAM_INT);
    $sth->bindParam(':nam',$_POST['name'],PDO::PARAM_STR);
    $stat = $sth->execute();
    break;
}

echo '<p><strong>';
if($stat === true){
    echo 'You have successfully added a new record to the database</strong></p>';
    system('/var/www/html/cron/updateJSON.pl');
}else{
    echo 'Record could not be added to the database</strong></p>';
    echo "<pre>Error:\n";
    print_r($sth->errorInfo());
    echo '</pre>';
}
?>
<p><a href="index.php">Return Home</a></p>
<p><a href="contrib.php">Make more contributions</a></p>
</div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
</div><!-- page-container -->
</body>
</html>
