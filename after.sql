-- Things to be added after initial sql added
-- after.sql
-- Henry J Schmale
-- July 26, 2015

-- Trigger to update the media table because certain record types require
-- special treatment. Like anything that is not a series
CREATE FUNCTION T_INSERT_MEDIA() RETURNS TRIGGER AS $$
  BEGIN
    IF NEW.MEDIA <> 3 AND NEW.SERIES IS FALSE THEN
      INSERT INTO EPISODES(MEDIAID,NAME)VALUES(NEW.ID,NEW.NAME);
    END IF;
    RETURN NEW;
  END;
$$ LANGUAGE PLPGSQL;

-- Function to insert into the media and episode table is a trigger
CREATE TRIGGER TRIG_INSERT_NEW_MEDIA AFTER INSERT ON MEDIAS
  FOR EACH ROW EXECUTE PROCEDURE T_INSERT_MEDIA();
