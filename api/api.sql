-- The sql to prepare the database for api usage
-- This sql should not be executed until I say so.

create or replace function random_string(length integer) returns text as 
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
    if length < 0 then
        raise exception 'Given length cannot be less than 0';
    end if;
    for i in 1..length loop
        result := result || chars[1+random()*(array_length(chars, 1)-1)];
    end loop;
    return result;
end;
$$ language plpgsql;

-- There are differint types of api requests
CREATE TABLE APIREQ_TYPES (
    id          INTEGER,
    descript    TEXT
);

INSERT INTO APIREQ_TYPES VALUES
    (1, 'listing of characters in series'),
    (2, 'listing of media items within a universe'),
    (3, 'character appearances')
;

CREATE TABLE APIKEY (
    id          SERIAL PRIMARY KEY,
    email       TEXT,                -- email of registrant
    ownername   TEXT,
    secret      CHAR(32) NOT NULL DEFAULT random_string(32), -- api accopyment
    usage       INTEGER DEFAULT 0,
    usagelimit  INTEGER DEFAULT 1000 -- max requests per day
);

-- API AUDIT TABLE
CREATE TABLE audit.APIREQUEST (
    id          SERIAL PRIMARY KEY
    reqtype     INTEGER REFERENCES APIREQ_TYPES(id),
    usrid       INTEGER REFERENCES APIKEY(id),
    value       TEXT
);
