<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
<title>Browse: Characters Database</title>
<meta name="description"
     content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
<script type="text/javascript" src="/scr/browse.js"></script>
<script type="text/javascript">window.onload = init;</script>
</head>

<body>
<div id="page-container">
<?php
include_once('inc/header.html');
?>

<div id="main-content">
<h1>Browse the Database</h1>
<?php include("inc/ads.html") ?>
<div id=form>
<form id=browse action="javascript:updateBrowse()" method="GET">
<select name=type>
<option value=1>Characters</option>
<option value=2>Media</option>
<option value=3>Actors</option>
</select>
<input type="submit" value="Submit" />
</form>
</div>

<div id="out">
</div>

<?php include("inc/ads.html") ?>

</div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
</div><!-- page-container -->
</body>
</html>

