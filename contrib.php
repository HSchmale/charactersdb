<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>Contribute: Characters Database</title>
<meta name="description"
    content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
<script src="/scr/contrib.js"></script>
<script>
window.onload = loadForm;
</script>
<style>
form{
width: 45%;
max-width: 350px;
margin: auto;
text-align: center;
}
</style>
</head>

<body>
<div id="page-container">
<?php
include_once('inc/header.html');
?>

<div id="main-content">
<h1>Contribute</h1>
<p>This project welcomes contributions from anyone. You can submit new data
using the appropriate form below. You can contribute just about anything
to this project, however this system does not serve images, because of the
additional bandwidth costs involved. The form below offers a method to
contribute single records to the database, and you can correct errors on the
browse page.</p>

<h1>Add a Database Record</h1>
<form id="contribAdd" name="contribAdd" method="POST" action="add.php" onsubmit="validate()">
Record Type:<br/><select name="add_t" onchange="updateFormType()">
<option value=1 selected="selected">Add Character Appearance</option>
<option value=2>Add Actor Appearance</option>
<option value=3>Add Actor</option>
<option value=4>Add Character</option>
<option value=5>Add Episode</option>
<option value=6>Add Media Item</option>
<option value=7>Add Universe</option>
<option value=8>Add Character Alias</option>
</select><br/>
<div id="entry">
</div>
<input disabled="true" type="submit" value="Submit" name="submit">
</form>
<p>The Submit button is enabled when all forms are properly filled
out.</p>
</div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
</div><!-- page-container -->
</body>
</html>
