#!/bin/bash
# cron/compress.sh
# A script for compressing the data files of the website, for faster transfer 
# of data to the client
#
# Henry J Schmale
# August 3, 2015

DOCROOT=/var/www/html/
COMPRESS_EXT=('*.json' '*.png' '*.html' '*.xml')

for ext in ${COMPRESS_EXT[*]}; do
    for file in $(find $DOCROOT -name $ext); do
        cat $file | gzip > ${file}.gz
    done
done
