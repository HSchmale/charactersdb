#!/usr/bin/perl
# cron/makeBrowse.pl
# Henry J Schmale
# August 2, 2015
#
# Makes the browse page.

use strict;
use warnings;
use DBI;
use File::Slurp;

# Open up the output file
open(my $FH, '>/var/www/html/browse.html') or die $!;
print $FH '<html><head><title>Browse the Database</title>';
print $FH '<link rel="stylesheet" href="/sty/main.css" media="screen">';
print $FH '<link rel="shortcut icon" href="/img/icon.png" />';
print $FH read_file('/var/www/html/inc/header.html');

# Write the page contents. This requires connecting to the database
my $dbh = DBI->connect("DBI:Pg:dbname=chardb;host=localhost", 
    "hschmale", "") or die "Connection Failure";
$dbh->disconnect();

print $FH read_file('/var/www/html/inc/footer.html');
close $FH;

