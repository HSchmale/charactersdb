#!/usr/bin/perl
# The Website Header Update Script. Updates the website header with the most
# recent stats about the number of records in the database. Runs every 4 hours
#
# Author:   Henry J Schmale
# Created:  July 20, 2015
# Modified: July 30, 2015

use strict;
use warnings;
use DBI;

my $outfile = '/var/www/html/inc/header.html';
open(my $FH, "> $outfile") or die ("Could not open file for writing!");

# Prepare the static content first
print $FH '<div class="center" id="header"><h1>The Fictional Characters '.
'Database</h1><p>Contains Records About</p><table><tr>';

# Database Connection
my $dbh = DBI->connect("DBI:Pg:dbname=chardb;host=localhost", "hschmale", "")
or die "Connection Failure";
my $sth = $dbh->prepare('SELECT * FROM RECORDSZ ORDER BY ID DESC LIMIT 1;');
$sth->execute();
my %hash;
while(my $ref = $sth->fetchrow_hashref()){
    print $FH '<th>'.$ref->{'appsnum'}.'<br/>Appearances</th>';
    print $FH '<th>'.$ref->{'charnum'}.'<br/>Characters</th>';
    print $FH '<th>'.$ref->{'medinum'}.'<br/>Medias</th>';
    print $FH '<th>'.$ref->{'univnum'}.'<br/>Universes</th>';
    print $FH '<th>'.$ref->{'episnum'}.'<br/>Episodes</th>';
    print $FH '<th>'.$ref->{'actonum'}.'<br/>Actors</th>';
}

# Close off the header
print $FH '</tr></table><hr/></div><table id="navbar"><tr>'.
'<th><a href="/index.php">Home</a></th>'.
'<th><a href="/about.php">About</a></th>'.
'<th><a href="/browse.php">Browse</a></th>'.
'<th><a href="/contrib.php">Contribute</a></th>'.
'</tr></table>';


$dbh->disconnect();
close $FH;
