#!/usr/bin/perl
# cron/peopleJSON.pl
# Prepares the JSON files for each character, actor and series so that a
# it can be requested via Javascript rather than hitting the database.
#
# Usage:
#
# ./cron/peopleJSON.pl
#       rebuilds all of the json files regarding people and medias
#
# ./cron/peopleJSON.pl <cat> <name>
#       rebuilds a specific json file from the database
#
#       <cat> between [1,3], the catorgory of item
#       <name> name of the item
#
# Author:   Henry J Schmale
# Created:  July 24, 2015
# Modified: July 30, 2015

use strict;
use warnings;
use DBI;
use JSON;

my %hashmap = (
        '1' => 'CHARACTERS',
        '2' => 'MEDIAS',
        '3' => 'ACTORS'
        );
my $DATADIR = '/var/www/html/dat/';
my $json = JSON->new->utf8;
$json->allow_nonref(1);
my $dbh = DBI->connect("DBI:Pg:dbname=chardb;host=localhost", "hschmale", "")
or die "Connection Failure";

if($#ARGV != 1){
# incorrect arg count just treat as no args were passed
    noargs();
}else{
    print $hashmap{$ARGV[0]}.'   '.$ARGV[1]."\n";
    my $sth = $dbh->prepare("SELECT id,name,descr,wikilink FROM $hashmap{$ARGV[0]} WHERE name LIKE ?");
    $sth->execute($ARGV[1]);
    while(my $r = $sth->fetchrow_hashref){
        if($ARGV[0] == 1){
# character
            makeCharacterJSON($r->{name}, $r->{descr}, $r->{id});
        }elsif($ARGV[0] == 2){
# media
            makeMediaJSON($r->{name}, $r->{descr}, $r->{wikilink}, $r->{id});
        }elsif($ARGV[0] == 3){
# actor
            makeActorJSON($r->{name}, $r->{descr}, $r->{wikilink}, $r->{id});
        }
    }
}

$dbh->disconnect();


sub noargs {
# Make the character json
    my $sth = $dbh->prepare('SELECT id,name,descr from CHARACTERS;');
    $sth->execute();
    while(my $row = $sth->fetchrow_hashref){
        makeCharacterJSON($row->{name},$row->{descr},$row->{id});
    }

# Make the actor json
    $sth = $dbh->prepare('SELECT * from ACTORS;');
    $sth->execute();
    while(my $row = $sth->fetchrow_hashref){
        makeActorJSON($row->{name}, $row->{descr},$row->{wikilink},$row->{id});
    }

# Make the series json
    $sth = $dbh->prepare('SELECT id,name,descr,wikilink from MEDIAS');
    $sth->execute();
    while(my $r = $sth->fetchrow_hashref){
        makeMediaJSON($r->{name}, $r->{descr}, $r->{wikilink}, $r->{id});
    }

# Make the misc. data array json
    makeJSONArray('chars', 'SELECT name from CHARACTERS;');
    makeJSONArray('medias', 'SELECT name FROM MEDIAS;');
    makeJSONArray('actors', 'SELECT name FROM ACTORS;');
}

sub makeJSONArray {
    my ($name, $q) = @_;
    my $outstr = "{\"$name\":";
    my (@array, $i);
    $i = 0;
    my $sth1 = $dbh->prepare($q);
    $sth1->execute();
    while(my $row = $sth1->fetchrow_hashref){
        $array[$i] = $row;
        $i++;
    }
    $outstr .= $json->encode(\@array);
    $outstr .= '}';
    my $file = $DATADIR.$name.'.json';
    open my $FH, ">$file" or die "Failed to open file: $!";
    print $FH $outstr;
    close $FH;
}

sub makeMediaJSON {
    my ($name, $desc, $link, $id) = @_;
    my $outstr = '{';
    my (@array,$i);
    $outstr .= "\"id\":".$json->encode($id).',';
    $outstr .= "\"name\":".$json->encode($name).',';
    $outstr .= "\"link\":".$json->encode($link).',';
    $outstr .= "\"desc\":".$json->encode($desc);
# add characters who appear in this media item
    my $sth1 = $dbh->prepare('SELECT act_name AS act,chr_name AS chr FROM vw_act_simp WHERE mid=?;');
    $sth1->execute($id);
    $i = 0; $#array = -1;
    while(my $r = $sth1->fetchrow_hashref){
        $array[$i] = $r;
        $i++;
    }
    $outstr .= ",\"chars\":".$json->encode(\@array);

    $name =~ s/\.//g;
    my $file = $DATADIR.$name.".json";
    $file =~ s/ /_/g;
    $outstr .= '}';
    open my $FH, ">$file" or die "Failed to open file: $!";
    print $FH $outstr;
    close $FH;
}

sub makeActorJSON {
    my ($name, $desc, $link, $id) = @_;
    my $outstr = '{';
    my (@array,$i);
    $outstr .= "\"id\":".$json->encode($id).',';
    $outstr .= "\"name\":".$json->encode($name).',';
    $outstr .= "\"link\":".$json->encode($link).',';
    $outstr .= "\"desc\":".$json->encode($desc).',';
    $outstr .= '"chars":';

# Actors who played this character
    my $sth1 = $dbh->prepare('SELECT DISTINCT CHRACT FROM VW_ACTAPPS WHERE actid=?;');
    $sth1->execute($id);
    $i = 0; $#array = -1;
    while(my $row = $sth1->fetchrow_hashref){
        $array[$i] = $row->{chract};
        $i++;
    }
    $outstr .= $json->encode(\@array).',"media":';
# Medias where this character appeared
    $sth1 = $dbh->prepare('SELECT DISTINCT SERIES_NAME FROM VW_ACTAPPS WHERE actid=?;');
    $sth1->execute($id);
    $i = 0; $#array = -1;
    while(my $row = $sth1->fetchrow_hashref){
        $array[$i] = $row->{series_name};
        $i++;
    }
    $outstr .= $json->encode(\@array);

    $name =~ s/\.//g;
    my $file = $DATADIR.$name.".json";
    $file =~ s/ /_/g;
    $outstr .= '}';
    open my $FH, ">$file" or die "Failed to open file: $!";
    print $FH $outstr;
    close $FH;
}

sub makeCharacterJSON {
    my ($name, $desc, $id) = @_;
    my $outstr = '{';
    my (@array,$i);
    my $sth1 = $dbh->prepare('SELECT NAME FROM CHRALIASES WHERE CHARID=?');
    $sth1->execute($id);
    $i = 0;
    while(my $row = $sth1->fetchrow_hashref){
        $array[$i] = $row->{name};
        $i++;
    }

    $outstr .= "\"id\":".$json->encode($id).',';
    $outstr .= "\"name\":".$json->encode($name).',';
    $outstr .= "\"desc\":".$json->encode($desc).',';
    $outstr .= '"alias":';
    $outstr .= $json->encode(\@array).',"actor":';

# Actors who played this character
    $sth1 = $dbh->prepare('SELECT DISTINCT ACTOR FROM VW_ACTAPPS WHERE CHRID=?;');
    $sth1->execute($id);
    $i = 0; $#array = -1;
    while(my $row = $sth1->fetchrow_hashref){
        $array[$i] = $row->{actor};
        $i++;
    }
    $outstr .= $json->encode(\@array).',"media":';
# Medias where this character appeared
    $sth1 = $dbh->prepare('SELECT DISTINCT SERIES_NAME FROM VW_CHRAPPS WHERE CHRID=?;');
    $sth1->execute($id);
    $i = 0; $#array = -1;
    while(my $row = $sth1->fetchrow_hashref){
        $array[$i] = $row->{series_name};
        $i++;
    }
    $outstr .= $json->encode(\@array);

    $name =~ s/\.//g;
    my $file = $DATADIR.$name.".json";
    $file =~ s/ /_/g;
    $outstr .= '}';
    open my $FH, ">$file" or die "Failed to open file: $!";
    print $FH $outstr;
    close $FH;
}
