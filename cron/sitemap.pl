#!/usr/bin/perl
# cron/sitemap.pl
# Henry J Schmale
# August 2, 2015
#
# Makes the xml sitemap

use strict;
use warnings;
use DBI;
use XML::LibXML;

# constatns are written in all caps
my $ROOT = 'http://www.fictionalcharactersdb.org/';
my $DOCROOT = '/var/www/html/';

# Global Array That Everything is added to
my @array;
my $arr_i = 0;
# Write the page contents. This requires connecting to the database
my $dbh = DBI->connect("DBI:Pg:dbname=chardb;host=localhost", 
    "hschmale", "") or die "Connection Failure";

# Add Normal Pages
addByName('');
addByName('index.php');
addByName('about.php');
addByName('browse.php');
addByName('contrib.php');

# Add the wiki pages
addBySql2Array('SELECT name FROM CHARACTERS',1,'weekly');
addBySql2Array('SELECT name FROM MEDIAS',2,'weekly');
addBySql2Array('SELECT name FROM ACTORS',3,'weekly');

# Write the site map
writeFile();

# Clean up
# Disconnect from the database cleanly.
$dbh->disconnect();

# Converts a page to a url
sub makeURL {
    my ($name, $cat) = @_;
    $name =~ s/ /_/g;
    return $ROOT . $cat . '/' . $name . '.html';
}

# add to array by name
sub addByName {
    my ($name) = @_;
    my %hash = ('loc' => "$ROOT$name",
                'changefreq' => 'monthly');
    $array[$arr_i] = \%hash;
    $arr_i++;
}

# Fetches by an sql query to add to sitemap
sub addBySql2Array {
    my ($sql,$typ,$chgfreq) = @_;
    my $sth = $dbh->prepare($sql);
    $sth->execute();
    while(my $r = $sth->fetchrow_hashref){
        my %hash;
        $hash{loc} = makeURL($r->{name}, $typ);
        $hash{changefreq} = $chgfreq;
        $array[$arr_i] = \%hash;
        $arr_i++;
    }
}

sub writeFile {
    my $doc = XML::LibXML::Document->new('1.0','utf-8');
    my $root = $doc->createElement('urlset');
    $root->setAttribute('xmlns' =>"http://www.sitemaps.org/schemas/sitemap/0.9");
    foreach(@array){
        my $url = $doc->createElement('url');
        my $loc = $doc->createElement('loc');
        my $chg = $doc->createElement('changefreq');
        $loc->appendTextNode($_->{loc});
        $chg->appendTextNode($_->{changefreq});
        $url->appendChild($loc);
        $url->appendChild($chg);
        $root->appendChild($url);
    }
    $doc->setDocumentElement($root);
    $doc->toFile("$DOCROOT/sitemap.xml");
}
