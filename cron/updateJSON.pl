#!/usr/bin/perl
# ./cron/updateJSON.pl
# The contrib json data file updater. The file name really isn't that
# accurate because this script only updates 1 json file, which is
# `contrib.json`.
#
# Author:   Henry J Schmale
# Created:  July 18, 2015
# Modified: July 30, 2015

use strict;
use warnings;
use File::Basename;
use File::Path qw/make_path/;
use DBI;
use JSON;

my $outfile = "/var/www/html/dat/contrib.json";
my $dir = dirname($outfile);
make_path($dir);

my $JSON = JSON->new->utf8;
my $dbh = DBI->connect("DBI:Pg:dbname=chardb;host=localhost", "hschmale", "")
or die "Connection Failure";

my $outstr = '{';

makeJSONArray('episodes', 'SELECT ID,NAME,MEDIAID FROM EPISODES ORDER BY NAME ASC');
$outstr .= ",";
makeJSONArray('characters', 'SELECT ID,NAME FROM CHARACTERS ORDER BY NAME ASC');
$outstr .= ",";
makeJSONArray('media', 'SELECT ID,NAME,UNIVID FROM MEDIAS ORDER BY NAME ASC');
$outstr .= ",";
makeJSONArray('actors', 'SELECT ID,NAME FROM ACTORS ORDER BY NAME ASC');
$outstr .= ",";
makeJSONArray('univid', 'SELECT ID,NAME FROM UNIVERSES ORDER BY ID ASC');
$outstr .= ",";
makeJSONArray('media_t', 'SELECT TYP FROM MEDIA_T');

# Output the data
open(my $FH, "> $outfile") or die "Could Not Open File For Writing!";
$outstr .= '}';
print $FH $outstr."\n";

close $FH;
$dbh->disconnect();

# Adds an item to the json array
sub makeJSONArray{
    my ($arrName,$q) = @_;
    $outstr .= "\"$arrName\":";
    my @junk;
    my $sth = $dbh->prepare($q);
    $sth->execute();
    my $i=0;
    while(my $ref = $sth->fetchrow_hashref()){
        $junk[$i] = $ref;
        $i++;
    }
    $outstr .= $JSON->encode(\@junk);
}
