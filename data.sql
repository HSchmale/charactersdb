-- data.sql
-- initial data to put into the database
-- Henry J Schmale
-- July 13, 2015


-- Add a couple of universes I can think of off the top of my head
INSERT INTO UNIVERSES(NAME,DESCR) VALUES
  ('No Shared Universe', NULL),
  ('Marvel','All of the Marvel Comics Universe and the Marvel Cinematic Universe'),
  ('DC','DC comics, movies, and animated universe'),
  ('Harry Potter','The universe in which all harry potter related stories take place');

INSERT INTO MEDIAS(NAME,MEDIA,UNIVID,SERIES,YEARAIRED) VALUES
  ('Iron Man: Armored Adventures',3,2,TRUE,2009), -- 1
  ('Iron Man',2,2,FALSE,2008),
  ('Supernatural',3,1,TRUE,2005),                 -- 3
  ('Justice League',3,3,TRUE,2001),
  ('Justice League: Unlimited',3,3,TRUE,2004),    -- 5
  ('Batman Beyond',3,3,TRUE,1999),
  ('The Avengers',2,2,FALSE,2012),                -- 7
  ('Harry Potter and the Philosopher''s Stone',1,4,FALSE,1997),
  ('Harry Potter and the Chamber of Secrets',1,4,FALSE,1999), -- 9
  ('Iron Man 2',2,2,FALSE,2010),
  ('Agents of S.H.I.E.L.D',3,2,TRUE,2013),        -- 11
  ('Iron Man 3',2,2,FALSE,2013),
  ('Avengers Assemble',3,2,TRUE,2013);

INSERT INTO CHARACTERS(NAME,UNIVID) VALUES
  ('Tony Stark', 2),      -- 1
  ('Howard Stark',2),
  ('James Rhodes', 2),    -- 3
  ('Pepper Pots', 2),
  ('Obadiah Stane',2),    -- 5
  ('Mandarin',2),
  ('Madame Masque',2),    -- 7
  ('Roberta Rhodes',2),
  ('Clark Kent',3),       -- 9
  ('Bruce Wayne',3),
  ('Hal Jorden',3),       -- 11
  ('Phil Coulson',2),
  ('Nick Fury',2),        -- 13
  ('Bruce Banner',2),
  ('Han Solo',5),         -- 15
  ('Jean Luc Picard',6),
  ('Benjamin Sisko',6),   -- 17
  ('Victor von Doom',2),
  ('Harold Hogan',2),     -- 19
  ('Justin Hammer',2),
  ('Natasha Romanoff',2), -- 21
  ('Clint Barton',2),
  ('T''Challa',2),        -- 23
  ('Grant Ward',2),
  ('Steve Rodgers',2),    -- 25
  ('Star Lord',2),
  ('Thanos',2);           -- 27

INSERT INTO CHRALIASES(CHARID,NAME) VALUES
  (1,'Iron Man'),
  (3,'War Machine'),
  (4,'Rescue'),
  (5,'Iron Monger'),
  (6,'Zhang'),
  (6,'Gene Kahn'),
  (3,'Rhodey'),
  (14,'The Incredible Hulk'),
  (11,'Green Lantern'),
  (9,'Superman'),
  (13,'Colonel Nicholas Joseph "Nick" Fury'),
  (7,'Whitney Frost'),(7,'Whitney Julietta Stane'),
  (19,'Happy'),
  (21,'Black Widow'),
  (20,'Titanium Man'),
  (22,'Hawkeye'),
  (23,'Black Panther');

INSERT INTO ACTORS(NAME,WIKILINK) VALUES
  ('Robert Downey Jr.','https://en.wikipedia.org/wiki/Robert_Downey_Jr.'),  -- 1
  ('Terrence Howard','https://en.wikipedia.org/wiki/Terrence_Howard'),
  ('Jeff Bridges','https://en.wikipedia.org/wiki/Jeff_Bridges'),            -- 3
  ('Adrian Petriw','https://en.wikipedia.org/wiki/Adrian_Petriw'),
  ('Daniel Bacon','https://en.wikipedia.org/wiki/Daniel_Bacon'),            -- 5
  ('Vincent Tong','https://en.wikipedia.org/wiki/Vincent_Tong'),
  ('Anna Cummer','https://en.wikipedia.org/wiki/Anna_Cummer'),              -- 7
  ('Mackenzie Gray','https://en.wikipedia.org/wiki/Mackenzie_Gray'),
  ('Jon Faveau','https://en.wikipedia.org/wiki/Jon_Favreau'),               -- 9
  ('Gwyneth Paltrow','https://en.wikipedia.org/wiki/Gwyneth_Paltrow'),
  ('Samuel L. Jackson','https://en.wikipedia.org/wiki/Samuel_L._Jackson'),  -- 11
  ('Scarlett Johansson','https://en.wikipedia.org/wiki/Scarlett_Johansson'),
  ('Sam Rockwell','https://en.wikipedia.org/wiki/Sam_Rockwell'),            -- 13
  ('Andrew Francis','https://en.wikipedia.org/wiki/Andrew_Francis');

INSERT INTO ACTAPPS(ACTID,CHRID,MEDIAID) VALUES
  (1,1,2),(1,1,12),(1,1,10),(1,1,7),(2,3,2),(3,5,2),(4,1,1),
  (5,3,1),(6,6,1),(7,4,1),(8,5,1),(9,19,10),(9,19,2),
  (10,4,2),(10,4,10),(10,4,7),(10,4,12),(11,13,10),(11,13,11),
  (12,21,10),(13,20,10),(14,22,1);

INSERT INTO EPISODES(SEASON,EPISODE,MEDIAID,NAME) VALUES
  (1,1,1,'Iron, Forged in Fire: Part 1'),  -- 1
  (1,2,1,'Iron, Forged in Fire: Part 2'),
  (1,3,1,'Secrets and Lies'), -- 3
  (1,4,1,'Cold War'),
  (1,5,1,'Whiplash'),  -- 5
  (1,6,1,'Iron Man vs. the Crimson Dynamo'),
  (1,7,1,'Meltdown'), -- 7
  (1,8,1,'Field Trip'),
  (1,9,1,'Ancient History 101'), -- 9
  (1,10,1,'Ready, A.I.M., Fire'),
  (1,11,1,'Seeing Red'), -- 11
  (1,12,1,'Masquerade'),
  (1,13,1,'Hide and Seek'), -- 13
  (1,14,1,'Man and Iron Man'),
  (1,15,1,'Panther''s Prey'), -- 15
  (1,16,1,'Fun with Lasers'),
  (1,17,1,'Chasing Ghosts'),  -- 17
  (1,18,1,'Pepper, Interrupted'),
  (1,19,1,'Technovore'), -- 19
  (1,20,1,'World on Fire'),
  (1,21,1,'Designed Only for Chaos'), -- 21
  (1,22,1,'Don''t Worry, Be Happy'),
  (1,23,1,'Uncontrollable'),  -- 23
  (1,24,1,'Best Served Cold'),
  (1,25,1,'Tales of Suspense: Part 1'), -- 25
  (1,26,1,'Tales of Suspense: Part 2'),
  (2,1,1,'The Invincible Iron Man, Part 1: Disassembled'), -- 27
  (2,2,1,'The Invincible Iron Man, Part 2: Reborn!'),
  (2,3,1,'Look into the Light'), -- 29
  (2,4,1,'Ghost in the Machine'),
  (2,5,1,'Armor Wars'), -- 31
  (2,6,1,'Line of Fire'),
  (2,7,1,'Titanium vs. Iron'), -- 33
  (2,8,1,'The Might of Doom'),
  (2,9,1,'The Hawk and the Spider'), -- 35
  (2,10,1,'Enter: Iron Monger'),
  (2,11,1,'Fugitive of S.H.I.E.L.D.'), -- 37
  (2,12,1,'All the Best People Are Mad'),
  (2,13,1,'Heavy Mettle'), -- 39
  (2,14,1,'Mandarin''s Quest'),
  (2,15,1,'Hostile Takeover'), -- 41
  (2,16,1,'Extremis'),
  (2,17,1,'The X-Factor'), -- 43
  (2,18,1,'Iron Man 2099'),
  (2,19,1,'Control-Alt-Delete'), -- 45
  (2,20,1,'Doomsday'),
  (2,21,1,'The Hammer Falls'), -- 47
  (2,22,1,'Rage of the Hulk'),
  (2,23,1,'Iron Monger Lives'), -- 49
  (2,24,1,'The Dragonseed'),
  (2,25,1,'The Makluan Invasion Part 1: Annihilate!'), -- 51
  (2,26,1,'The Makluan Invasion Part 2: Unite!'),
  (0,0,2,'Iron Man'),  -- 53
  (0,0,10,'Iron Man 2'),
  (0,0,7,'The Avengers'), -- 55
  (1,1,11,'Pilot'),  -- Agents of shield
  (1,2,11,'0-8-4'), -- 57
  (1,3,11,'The Asset'),
  (1,4,11,'Eye Spy'), -- 59
  (1,5,11,'Girl in the Flower Dress'),
  (1,6,11,'FZZT'), -- 61
  (1,7,11,'The Hub'),
  (1,8,11,'The Well'), -- 63
  (1,9,11,'Repairs'),
  (1,10,11,'The Bridge'), -- 65
  (1,11,11,'The Magical Place'),
  (1,12,11,'Seeds'), -- 67
  (1,13,11,'T.R.A.C.K.S.'),
  (1,14,11,'T.A.H.I.T.I.'), -- 69
  (1,15,11,'Yes Men'),
  (1,16,11,'End of the Beginning'), -- 71
  (1,17,11,'Turn, Turn, Turn'),
  (1,18,11,'Providence'), -- 73
  (1,19,11,'The Only Light in the Darkness'),
  (1,20,11,'Nothing Personal'), -- 75
  (1,21,11,'Ragtag'),
  (1,22,11,'Beginning of the End'), -- 77
  (2,1,11,'Shadows'),
  (2,2,11,'Heavy is the Head'), --79
  (2,3,11,'Making Friends and Influencing People'),
  (2,4,11,'Face My Enemy'), -- 81
  (2,5,11,'A Hen in the Wolf House'),
  (2,6,11,'A Fractured House'), -- 83
  (2,7,11,'The Writing on the Wall'),
  (2,8,11,'The Things We Bury'), -- 85
  (2,9,11,'...Ye Who Enter Here'),
  (2,10,11,'What They Become'), -- 87
  (2,11,11,'Aftershocks'),
  (2,12,11,'Who You Really Are'), -- 89
  (2,13,11,'One of Us'),
  (2,14,11,'Love in the Time of Hydra'), --91
  (2,15,11,'One Door Closes'),
  (2,16,11,'Afterlife'), -- 93
  (2,17,11,'Melinda'),
  (2,18,11,'The Frenemy of My Enemy'), --95
  (2,19,11,'The Dirty Half Dozen'),
  (2,20,11,'Scars'), -- 97
  (2,21,11,'S.O.S. Part 1'),
  (2,22,11,'S.O.S. Part 2'), -- 99
  (0,0,12,'Iron Man 3'),
  (1,1,13,'The Avengers Protocol Part 1'), -- 101
  (1,2,13,'The Avengers Protocol Part 2'),
  (1,3,13,'Ghost of a Chance'), -- 103
  (1,4,13,'The Serpent of Doom'),
  (1,5,13,'Blood Feud'), -- 105
  (1,6,13,'Super-Adaptoid'),
  (1,7,13,'Hyperion'), -- 107
  (1,8,13,'Molecule Kid'),
  (1,9,13,'Depth Charge'), -- 109
  (1,10,13,'The Doomstroyer'),
  (1,11,13,'Hulked Out Heros'), -- 111
  (1,12,13,'Avengers: Impossible'),
  (1,13,13,'In Deep'), -- 113
  (1,14,13,'Hulk''s Day Out'),
  (1,15,13,'Planet Doom'), -- 115
  (1,16,13,'Bring on the Bad Guys'),
  (1,17,13,'Savages'), -- 117
  (1,18,13,'Mojo World'),
  (1,19,13,'The Ambassador'), -- 119
  (1,20,13,'All-Father''s Day'),
  (1,21,13,'By the Numbers'), -- 121
  (1,22,13,'Guardians and Space Knights'),
  (1,23,13,'One Little Thing'), -- 123
  (1,24,13,'Crime and Circuses'),
  (1,25,13,'Exodus'), -- 125
  (1,26,13,'The Final Showdown'),
  (2,1,13,'The Arsenal'), -- 127
  (2,2,13,'Thanos Rising'),
  (2,3,13,'Valhalla Can Wait'), -- 129
  (2,4,13,'Ghosts of the Past'),
  (2,5,13,'Beneath the Surface'), -- 131
  (2,6,13,'Nighthawk'),
  (2,7,13,'The Age of Tony Stark'), -- 133
  (2,8,13,'Head to Head'),
  (2,9,13,'The Dark Avengers'), -- 135
  (2,10,13,'Back to the Learning Hall'),
  (2,11,13,'Downgraded'), -- 137
  (2,12,13,'Widow''s Run'),
  (2,13,13,'Thanos Triumphant'), -- 139
  (2,14,13,'Crack in the System'),
  (2,15,13,'Avengers Disassembled'), -- 141
  (2,16,13,'Small Time Heroes'),
  (2,17,13,'Secret Avengers'), -- 143
  (2,18,13,'The Ultron Outbreak'),
  (2,19,13,'The New Guy'), -- 145
  (2,20,13,'Terminal Velocity'),
  (2,21,13,'Spectrums'), -- 147
  (2,22,13,'Midgard Crisis'),
  (2,23,13,'Avengers'' Last Stand'), -- 149
  (2,24,13,'Avengers Underground');

INSERT INTO CHRAPPS(CHARID,MEDIAID,EPISODEID) VALUES
  (1,13,101),(14,13,101),(25,13,101),(21,13,101),(22,13,101),
  (1,13,102),(14,13,102),(25,13,102),(21,13,102),(22,13,102),
  (1,13,103),(14,13,103),(25,13,103),(21,13,103),(22,13,103),
  (1,13,104),(14,13,104),(25,13,104),(21,13,104),(22,13,104),
  (1,13,105),(14,13,105),(25,13,105),
  (1,13,106),(14,13,106),(25,13,106),
  (1,13,107),(14,13,107),(25,13,107),
  (1,13,108),(14,13,108),(25,13,108),
  (1,13,109),(14,13,109),(25,13,109),
  (1,13,110),(14,13,110),(25,13,110),
  (1,13,111),(14,13,111),(25,13,111),
  (1,13,112),(14,13,112),(25,13,112),
  (1,13,113),(14,13,113),(25,13,113),
  (1,13,114),(14,13,114),(25,13,114),
  (1,13,115),(14,13,115),(25,13,115),
  (1,13,116),(14,13,116),(25,13,116),
  (1,13,117),(14,13,117),(25,13,117),
  (1,13,118),(14,13,118),(25,13,118),
  (1,13,119),(14,13,119),(25,13,119),
  (1,13,120),(14,13,120),(25,13,120),
  (1,13,121),(14,13,121),(25,13,121),
  (1,13,122),(14,13,122),(25,13,122),
  (1,13,123),(14,13,123),(25,13,123),
  (1,13,124),(14,13,124),
  (1,13,125),
  (1,13,126),
  (1,13,127),
  (1,13,128),
  (1,13,129),
  (1,13,130),
  (1,13,131),
  (1,13,132),
  (1,13,133),
  (1,13,134),
  (1,13,135),
  (1,13,136),
  (1,13,137),
  (1,13,138),
  (1,13,139),
  (1,13,140),
  (1,13,141),
  (1,13,142),
  (1,13,143),
  (1,13,144),
  (1,13,145),
  (1,13,146),
  (1,13,147),
  (1,13,148),
  (1,13,149),
  (1,13,150),
  (1,2,53),(3,2,53),(4,2,53),(5,2,53),(12,2,53),(19,2,53),  -- Iron Man 2008
  (1,10,54),(19,10,54),(4,10,54),(3,10,54),(20,10,54),(21,10,54),
  (1,12,100),(4,12,100),(3,12,100),(6,12,100),
  (1,1,1),(2,1,1),(3,1,1),(4,1,1),(5,1,1),(6,1,1),(8,1,1),  -- IMAA 1
  (1,1,2),(2,1,2),(3,1,2),(4,1,2),(5,1,2),(6,1,2),          -- IMAA 2
  (1,1,3),(3,1,3),(4,1,3),                                  -- IMAA 3
  (1,1,4),(3,1,4),(4,1,4),(5,1,4),                          -- IMAA 4
  (1,1,5),(3,1,5),(4,1,5),
  (1,1,6),(3,1,6),(4,1,6),(5,1,6),
  (1,1,7),(3,1,7),(4,1,7),(2,1,7),
  (1,1,8),(3,1,8),(4,1,8),
  (1,1,9),(3,1,9),(4,1,9),
  (1,1,10),(3,1,10),(4,1,10),
  (1,1,11),(3,1,11),(4,1,11),(5,1,11),
  (1,1,12),(3,1,12),(4,1,12),(7,1,12),(5,1,12),
  (1,1,13),(3,1,13),(4,1,13),
  (1,1,14),(3,1,14),(4,1,14),
  (1,1,15),(3,1,15),(4,1,15),(23,1,15),
  (1,1,16),(3,1,16),(4,1,16),(13,1,16),
  (1,1,17),(3,1,17),(4,1,17),(7,1,17),(5,1,17),
  (1,1,18),(3,1,18),(4,1,18),
  (1,1,19),(3,1,19),(4,1,19),(13,1,19),
  (1,1,20),(3,1,20),(4,1,20),
  (1,1,21),(3,1,21),(4,1,21),(13,1,21),
  (1,1,22),(3,1,22),(4,1,22),(7,1,22),(19,1,22),
  (1,1,23),(3,1,23),(4,1,23),(14,1,23),
  (1,1,24),(3,1,24),(4,1,24),(7,1,24),(5,1,24),
  (1,1,25),(3,1,25),(4,1,25),
  (1,1,26),(3,1,26),(4,1,26),
  (1,1,27),(3,1,27),(4,1,27),
  (1,1,28),(3,1,28),(4,1,28),
  (1,1,29),(3,1,29),(4,1,29),
  (1,1,30),(3,1,30),(4,1,30),(5,1,30),
  (1,1,31),(3,1,31),(4,1,31),(5,1,31),
  (1,1,32),(3,1,32),(4,1,32),(23,1,32),
  (1,1,33),(3,1,33),(4,1,33),(13,1,33),
  (1,1,34),(3,1,34),(4,1,34),(5,1,34),(18,1,34),
  (1,1,35),(3,1,35),(4,1,35),(21,1,35),(20,1,35),(22,1,35),
  (1,1,36),(3,1,36),(4,1,36),
  (1,1,37),(3,1,37),(4,1,37),(13,1,37),(21,1,37),
  (1,1,38),(3,1,38),(4,1,38),(19,1,38),
  (1,1,39),(3,1,39),(4,1,39),
  (1,1,40),(3,1,40),(4,1,40),
  (1,1,41),(3,1,41),(4,1,41),
  (1,1,42),(3,1,42),(4,1,42),(13,1,42),
  (1,1,43),(3,1,43),(4,1,43),
  (1,1,44),(3,1,44),(4,1,44),(21,1,44),(22,1,44),
  (1,1,45),(3,1,45),(4,1,45),
  (1,1,46),(3,1,46),(4,1,46),(18,1,46),
  (1,1,47),(3,1,47),(4,1,47),
  (1,1,48),(3,1,48),(4,1,48),(14,1,48),
  (1,1,49),(3,1,49),(4,1,49),(7,1,49),
  (1,1,50),(3,1,50),(4,1,50),
  (1,1,51),(3,1,51),(4,1,51),(13,1,51),(2,1,51),(21,1,51),(23,1,51),
  (1,1,52),(3,1,52),(4,1,52),(2,1,52),(13,1,52),(21,1,52),(22,1,52),(23,1,52),
  (12,11,56),(24,11,56),                       -- Agents of Shield
  (12,11,57),(13,11,57),(24,11,57),
  (12,11,58),(24,11,58),
  (12,11,59),(24,11,59),
  (12,11,60),(24,11,60),
  (12,11,61),(24,11,61),
  (12,11,62),(24,11,62),
  (12,11,63),(24,11,63),
  (12,11,64),(24,11,64),
  (12,11,65),(24,11,65),
  (12,11,66),(24,11,66),
  (12,11,67),(24,11,67),
  (12,11,68),(24,11,68),
  (12,11,69),(24,11,69),
  (12,11,70),(24,11,70),
  (12,11,71),(24,11,71),
  (12,11,72),(24,11,72),
  (12,11,73),(24,11,73),
  (12,11,74),(24,11,74),
  (12,11,75),(24,11,75),
  (12,11,76),(24,11,76),
  (12,11,77),(24,11,77),
  (12,11,78),
  (12,11,79),
  (12,11,80),
  (12,11,81),
  (12,11,82),
  (12,11,83),
  (12,11,84),
  (12,11,85),
  (12,11,86),
  (12,11,87),
  (12,11,88),
  (12,11,89),
  (12,11,90),
  (12,11,91),
  (12,11,92),
  (12,11,93),
  (12,11,94),
  (12,11,95),
  (12,11,96),
  (12,11,97),
  (12,11,98),
  (12,11,99),(13,11,99);

UPDATE MEDIAS SET DESCR='A teenage tony stark becomes Iron Man.' WHERE ID=1;
UPDATE MEDIAS SET DESCR='Tony Stark is attacked by terrorists and has a revelation about violence.' WHERE ID=2;
UPDATE CHARACTERS SET DESCR='Supporting character. Has been a bodyguard, boxer and chauffer. Usually is some kind of athlete.' WHERE ID=19;
UPDATE CHARACTERS SET DESCR='Rular of Latvaria. Enemy of Reed Richards.' WHERE ID=18;
UPDATE CHARACTERS SET DESCR='Station commander of Deep Space 9. Very important figure in the domain war. Emisary of the prophets' WHERE ID=17;
UPDATE CHARACTERS SET DESCR='Captain of the USS Enterprise-D.' WHERE ID=16;
UPDATE CHARACTERS SET DESCR='Captain of the Millianium Falcon' WHERE ID=15;
UPDATE CHARACTERS SET DESCR='Transformed into the HULK by a GAMMA Bomb while rescuing Rick Jones' WHERE ID=14;
UPDATE CHARACTERS SET DESCR='The Director of S.H.I.E.L.D.' WHERE ID=13;
UPDATE CHARACTERS SET DESCR='A top-level S.H.I.E.L.D agent. First appeared in the 2008 Iron Man Film.' WHERE ID=12;
UPDATE CHARACTERS SET DESCR='A member of the Green Lantern Corps, and a test fighter pilot.' WHERE ID=11;
UPDATE CHARACTERS SET DESCR='His parents were killed before his very eyes when he was a kid. This prompted him to become the batman.' WHERE ID=10;
UPDATE CHARACTERS SET DESCR='Billionare Inventor and CEO of Stark Industries' WHERE ID=1;
UPDATE CHARACTERS SET DESCR='Father of Tony Stark and Founder of Stark Industries' WHERE ID=2;
UPDATE CHARACTERS SET DESCR='Friend of Tony Stark' WHERE ID=3;
UPDATE CHARACTERS SET DESCR='Secretary of Tony Stark. Later becomes CEO of Stark Industries' WHERE ID=4;
UPDATE CHARACTERS SET DESCR='Villain who typically tries to sieze control over Stark Industries' WHERE ID=5;
UPDATE CHARACTERS SET DESCR='Villain who has power rings and tries to take over the world. He is from China' WHERE ID=6;
UPDATE CHARACTERS SET DESCR='Former lover of Tony Stark, now tries to kill him for many reason. Typically is mentally ill' ||
  ' in many series, due to grief. Usually wears a gold mask to cover her disfigured face which might have been caused by acid.' WHERE ID=7;
UPDATE CHARACTERS SET DESCR='James Rhodes''s Mom in Iron Man: Armored Adventures' WHERE ID=8;
UPDATE CHARACTERS SET DESCR='Works at the Daily Planet in Metropolis. Vurnable to krptonite' WHERE ID=9;

-- Finish Up the database by optimizing it
ANALYSE;
