<?php
include('inc/db.php');
include('inc/const.php');

$min = 1; $max = 3;
$value = $_POST['p'];
if(!($min <= $value) && ($value <= $max)){
   die('Go Back');
}

$table = $INFO_UPDATE[$_POST['p']];

if($table === null){
    die('You should NOT be here. Go Back.');
}

$sql = "UPDATE $table SET DESCR=:desc WHERE NAME=:name;";
$sth = $dbh->prepare($sql);
$sth->bindParam(':desc', $_POST['d'], PDO::PARAM_STR);
$sth->bindParam(':name', $_POST['n'], PDO::PARAM_STR);
$stat = $sth->execute();

if($stat === true){
    echo 'PASS';
    $cmd = 'perl /var/www/html/cron/peopleJSON.pl '.$_POST['p'].' \''.$_POST['n'].'\'';
    system($cmd);
}else{
    echo "FAIL\n";
    print_r($sth->errorInfo());
}
