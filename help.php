<!DOCTYPE html>
<html>
<head>
<title>Help: Characters Database</title>
<meta name="description"
     content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
</head>

<body>
<div id="page-container">
<?php
include_once('inc/header.html');
?>

<div id="main-content">
<h1>Help</h1>
</div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
</div><!-- page-container -->
</body>
</html>

