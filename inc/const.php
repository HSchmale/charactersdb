<?php
// inc/const.php
// Constants for the php scripts

// Add Record Type Constants
define("ADDCHRAPP", 1);     // Add Character Appearance
define("ADDACTAPP", 2);     // Add Actor Appearance
define("ADDACT", 3);        // Add an actor
define("ADDCHR", 4);        // Add a character
define("ADDEPI", 5);        // Add an episode to a media item
define("ADDMED", 6);        // Add a media item
define("ADDUNI", 7);        // Add Another Shared Universe
define("ADDCHRALIAS", 8);   // Add an alias for a character

// Media Types Look Up Array
$MEDIA_TYPES = array(
    0 => 'ANY',
    1 => 'BOOK',
    2 => 'MOVIE',
    3 => 'TV SHOW',
    4 => 'VIDEO GAME',
    5 => 'COMICS');

$SEARCH_T = array(
    'chr' => 'Character',
    'act' => 'Actor'
);

$INFO_UPDATE = array(
    '1' => 'CHARACTERS',
    '2' => 'MEDIAS',
    '3' => 'ACTORS'
);
