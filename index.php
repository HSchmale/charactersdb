<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
<title>Characters Database</title>
<meta name="description"
content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
<meta name="msvalidate.01" content="975F95807D670C15E30D60749AD6F6F1" />
</head>

<body>
<div id="page-container">
<?php
    include_once('inc/header.html');
?>

<div id="main-content">
<p>This database is devoted to finding appearances of characters from the same
fictional universe, and listing their appearances, in all forms of media. This
project was started because it's annoying trying to figure out what episode
of a tv series a certain character shows up in, and this project is meant to
remedy this. This project depends on volunteers to add series, works and
appearances, so won't you please help.</p>

<?php include("inc/ads.html"); ?>

<h1 class="center">Search The Database</h1>
<?php include_once('inc/searchform.html'); ?>

    <h2 class="center">Instructions</h2>
    <p>Make a query using the form above. In the query field enter a character
    name. In the media field, select what appearances of that character you are
    looking for. Press the submit button when done.</p>
    </div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
    </div><!-- page-container -->
    </body>
    </html>

