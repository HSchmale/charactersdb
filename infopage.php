<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title><?php echo $_GET['c'],': '; ?>Characters Database</title>
<meta name="description"
     content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="stylesheet" href="/sty/infopage.css" />
<link rel="shortcut icon" href="/img/icon.png" />
<script src="https://cdn.rawgit.com/showdownjs/showdown/1.2.1/dist/showdown.min.js"></script>
<script src="/scr/detailpage.js"></script>
<script>
window.onload=onPageLoad;
</script>
<style>
#main-content{
    max-width: 800px;
    margin: 0 auto;
}
</style>
</head>

<body>
<div id="page-container">
<?php
include_once('inc/header.html');
?>

<div id="main-content">
</div><!-- main-content -->

<?php
include('inc/ads.html');
include_once('inc/footer.html');
?>
</div><!-- page-container -->
</body>
</html>

