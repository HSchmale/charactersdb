# Characters Database Makefile
# Henry J Schmale
# Modified: July 18, 2015
# Created:  July 14, 2015

SHELL      = /bin/bash
DBNAME     = chardb
DBUSRNAME  = hschmale
DEPLOC     = /var/www/html
RELLOC     = /usr/share/nginx/html

# Image Handling
IMGSRC     = $(shell find imgsrc -name '*.xcf')
# Magic to change files from imgsrc/%.xcf to img/%.png
IMGOUT     = $(patsubst imgsrc/%,img/%,$(patsubst %.xcf,%.png,$(IMGSRC)))

# clear built in rules
.SUFFIXES:

all: test inc/header.html

db.tmp: setup.sql data.sql after.sql
	dropdb $(DBNAME)
	createdb --owner=$(DBUSRNAME) $(DBNAME)
	cat $^ | psql $(DBNAME)
	touch $@

release: $(IMGOUT)  json
	rsync -v -r --exclude '*.tmp' --exclude '*.git*' --exclude 'makefile' \
	--exclude 'imgsrc/*' --exclude '*.sql' --exclude '*.md' \
	--exclude scraper/* . $(DEPLOC)

test: $(IMGOUT) db.tmp json
	rsync -v -r --exclude '*.tmp' --exclude '*.git*' --exclude 'makefile' \
	--exclude 'imgsrc/*' --exclude '*.sql' --exclude '*.md' . $(DEPLOC)

clean:
	rm -f db.tmp
	rm -f $(IMGOUT)

# Target For PNG
# Source files are in imgsrc, and output files are placed in img
img/%.png: imgsrc/%.xcf
	$(info Building $@)
	convert $< -layers merge $@

.PHONY: json
json: db.tmp
	if [ ! -d $(DEPLOC)/dat ]; then mkdir $(DEPLOC)/dat ; fi
	chmod 777 $(DEPLOC)/dat
	./cron/updateJSON.pl
	./cron/peopleJSON.pl
	chmod 666 $(DEPLOC)/dat/*

inc/header.html:
	./cron/makeHeader.pl

.PHONY: test
