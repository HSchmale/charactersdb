# The Fictional Characters Database
This is a database of characters and their appearances in various forms of
media. It is designed to be a better version of IMDB for indexing
characters and actors, and in which media they appear in.

# Notes
The below are notes about what needs to be setup on the running server.

## Cron Jobs
* rebuild all of the JSON at least once a day
* Update the website header every 6 hours everyday
* Back up the database on S3 every 2 days 

`````
1 1 * * * /var/www/html/cron/updateJSON.pl
2 1 * * * /var/www/html/cron/peopleJSON.pl
0 0,6,12,18 * * * /var/www/html/cron/makeHeader.pl
`````

## Rewrite Rules
* `/<catnum>/<name>.html` -> `/infopage.php?p=<catnum>&c=<name>`

`````
location / {
    rewrite ^/([^/]*)/([^/]*).html$ /infopage.php?p=$1&c=$2 last;
}
`````
