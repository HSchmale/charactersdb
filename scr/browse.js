/* scr/browse.js
 * The browse page js
 * Henry J Schmale
 * July 30, 2015
 */

var ENUM_CATG = {
    "1":"/dat/chars.json",
    "2":"/dat/medias.json",
    "3":"/dat/actors.json"
};

var json = null;
var typeSelect = function(){return document.forms['browse']['type'].value;};

function init(){
    console.log('init() called');
    updateBrowse();
}

function updateBrowse(){
    var getthis = ENUM_CATG[typeSelect()];
    var http = new XMLHttpRequest();

    http.onreadystatechange = function(){
        if(http.readyState == 4 && http.status == 200){
            json = JSON.parse(http.responseText);
            console.log('Got the JSON');
            writeList();
        }
    }

    http.open('GET', getthis, true);
    http.send(null);
}

function writeList(){
    var out = document.getElementById('out');
    var txt = "<ul>";
    switch(typeSelect()){
    case "1":
        for(var i = 0; i < json.chars.length; i++){
            txt += '<li><a href=' + writeURL(json.chars[i].name,1) + '>' + 
                json.chars[i].name + '</a></li>';
        }
        break;
    case "2":
        for(var i = 0; i < json.medias.length; i++){
            txt += '<li><a href=' + writeURL(json.medias[i].name,2) + '>' + 
                json.medias[i].name + '</a></li>';
        }
        break;
    case "3":
        for(var i = 0; i < json.actors.length; i++){
            txt += '<li><a href=' + writeURL(json.actors[i].name,3) + '>' + 
                json.actors[i].name + '</a></li>';
        }
        break;
    }
    txt += '</ul>';
    out.innerHTML = txt;
}

function writeURL(c,p){
    return '/' + p + '/' + c.replace(/ /g,'_') + '.html';
}
