/* File:    scr/contrib.js
 * Brief:   Javascript for the contributions page and modifying that form
 * Author:  Henry J Schmale
 * Created: July 18, 2015
 */
"use strict"

var xmlHttp=null;
var json=null;
var formElm=null;

function loadTheJSON(){
    if(xmlHttp.readyState == 4 && xmlHttp.status == 200 ){
        json = JSON.parse(xmlHttp.responseText);
    }
}

function loadForm(){
    var url="/dat/contrib.json";
    xmlHttp=new XMLHttpRequest();
    xmlHttp.onreadystatechange = loadTheJSON;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
    setTimeout(updateFormType,200); // wait for http req to complete
}

/* Returns the code to make a character selection box
 * with the options provided by the recieved JSON
 */
function getCharacterSelectCode(){
    var str,i;
    str += '<option value=0>NULL</option>';
    for(i=0;i<json.characters.length;i++){
        str += "<option value=\""+json.characters[i].id+"\">"+
            json.characters[i].name+"</option>";
    }
    if(document.getElementById('chr') != null){
        document.getElementById('chr').innerHTML = str;
    }
}

function getMediaSelectCode(){
    var str, i;
    str += '<option value=0>NULL</option>';
    for(i=0;i<json.media.length;i++){
        str += "<option value=\""+json.media[i].id+"\">"+
            json.media[i].name+"</option>";
    }
    if(document.getElementById('med') != null){
        document.getElementById('med').innerHTML = str;
    }
}

function getUniverseSelectCode(){
    var str, i;
    for(i=0;i<json.univid.length;i++){
        str += "<option value=\""+json.univid[i].id+"\">"+
            json.univid[i].name+"</option>";
    }
    if(document.getElementById('uni') != null){
        document.getElementById('uni').innerHTML = str;
    }
}

function getActorSelectCode(){
    var str, i;
    str += '<option value=0>NULL</option>';
    for(i=0;i<json.actors.length;i++){
        str += "<option value=\""+json.actors[i].id+"\">"+
            json.actors[i].name+"</option>";
    }
    if(document.getElementById('actor') != null){
       document.getElementById('actor').innerHTML = str;
    }
}

function addTextInput(name, label){
    return label + ':<br/><input type=\"text\" name=\"' + name + 
        '\" id=\"' + name + '\" onchange=\"validate()\"><br/>';
}

function addInput(name,label,type){
    return label+':<br/><input type=\"' + type + '\" name=\"' + name +
        '\" id=\"' + name + '\" onchange=\"validate()\" value=\"0\"><br/>';
}

function getMediaTypesSelect(){
     return 'Media Type:<br/><select id="media_t" name="media_t">' +
     '<option value=1>Book</option><option value=2>Movie</option>' +
     '<option value=3>Tv Show</option><option value=4>Video Game</option>' +
     '<option value=5>Comics</option></select>';
}

function getSelect(name,label,onchange){
    return label + ':<br/><select id="' + name + '" name="' + name + 
    '" onchange="' + onchange + '"></select>';
}

function getOptionCode(val, text){
    return '<option value="' + val + '">' + text + '</option>';
}

function updateEpisodes(){
    if(document.forms['contribAdd']['med'] == null){return;}
    var mediaid = document.forms['contribAdd']['med'].value;
    var temp, i;
    temp += '<option value=0>NULL</option>';
    for(i=0;i<json.episodes.length;i++){
        if(json.episodes[i].mediaid == mediaid){
            temp += getOptionCode(json.episodes[i].id, json.episodes[i].name);
        }
    }
    document.getElementById('epi').innerHTML = temp;
}

/* Changes the contribAdd form to the appropite type for the selected form
 * type. Which record does one want to add?
 */
function updateFormType(){
    var formState=document.forms["contribAdd"]["add_t"].value;
    var elm=document.getElementById("entry");
    var out=""; 
    var sub = document.forms['contribAdd']['submit'];
    sub.disabled=true;
    switch(formState){
    case "1": // Add Chr app
        out += getSelect('med','Media Name', 'updateEpisodes()');
        out += getSelect('chr','Character Name','validate()');
        out += getSelect('epi','Episode Name','validate()');
        out += addTextInput('notes','Notes');
        break;
    case "2": // Add Actor appearance
        out += getSelect('med','Media Name', 'validate()');
        out += getSelect('chr','Character Name','validate()');
        out += getSelect('actor','Actor Name', 'validate()');
        break;
    case "3": // Add Actor
        out += addTextInput('name','Actor Name');
        out += addTextInput('wiki','Source Page: ');
        break;
    case "4": // Add Character
        out += getSelect('uni','Universe Name', 'validate()');
        out += addTextInput('name','Character Name');
        out += addTextInput('desc','Description');
        break;
    case "5": // Add Episode
        out += getSelect('med','Media Name', 'validate()');
        out += addTextInput('name','Name');
        out += addInput('season','Season','number');
        out += addInput('epinum','Episode Number','number');
        out += addInput('airdate','Air Date(MM/DD/YYYY)','date');
        out += addTextInput('desc','Synopsis');
        break;
    case "6": // Add Media Item
        out += getMediaTypesSelect();
        out += getSelect('uni','Universe Name', 'validate()');
        out += addTextInput('name','Media Name');
        out += addTextInput('wiki','Source Link');
        out += addTextInput('desc','Description');
        out += addInput('year','Year Published','number');
        out += addInput('series','Is Series?','checkbox');
        break;
    case "7": // Add Universe
        out += addTextInput('name','Universe Name');
        out += addTextInput('desc','Description');
        break;
    case "8": // Add Character Alias
        out += getSelect('chr','Character Name','validate()');
        out += addTextInput('name','Alias');
        break;
    }
    elm.innerHTML=out;
    getMediaSelectCode();
    getCharacterSelectCode();
    getActorSelectCode();
    getUniverseSelectCode();
    updateEpisodes();
    validate();
}

/* Validates the current form. If the fields are valid then the Submit 
 * button is enabled
 */
function validate(){
    var sels = document.getElementsByTagName('select');
    var sub = document.forms['contribAdd']['submit'];
    var name = document.getElementById('name');
    var i;
    for(i = 0; i < sels.length; i++){
        if(sels[i].value == "0"){
            sub.disabled = true;
            return;
        }
    }
    if(name == null){
        sub.disabled=false;
        return;
    }
    if(name.value == ""){
        sub.disabled = true;
        return;
    }
    sub.disabled=false;
}
