/* scr/detailpage.js
 * The Javascript for the detail page.
 * This goes on the character,actor and series page. It gets the json for
 * generating a details page, and then updates the page to do so.
 * Henry J Schmale
 * July 24, 2015
 */

var xmlHttp = null;
var json = null;
var params = null;

function getSearchParameters() {
    var str = window.location.href.split('/');
    var p = new Array();
    p['p'] = str[3];
    p['c'] = str[4].lastIndexOf('.');
    p['c'] = str[4].substr(0,p['c']);
    console.log(p);
    console.log(str);
    console.log(p.c);
    return p;
}

function loadJSON(){
    if(xmlHttp.readyState == 4 && xmlHttp.status == 200 ){
        json = JSON.parse(xmlHttp.responseText);
        console.log(json);
    }
}

function makeJSONRequest(url){
    xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = loadJSON;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function makePageHeader(){
    var head = '<h1>';
    switch(params.p){
        case '1': // character
            head += 'Character - ';
            break;
        case '2':
            head += 'Media - ';
            break;
        case '3':
            head += 'Actor - ';
            break;
        default:
            break;
    }
    if(("link" in json)){
        if(json.link !== null){
            head += '<a href="' + json.link + '">' + json.name + '</a>';
        }else{
            head += json.name;
        }
    }else{
        head += json.name;
    }
    head += '</h1>';
    return head;
}

function itemizeArray(array){
    var txt='',i;
    for(i = 0; i < array.length; i++){
        txt += '<li>' + array[i] + '</li>';
    }
    return txt;
}

function writeCharacterPage(){
    var txt = '', i;
    if((json.alias != null) && (json.alias.length > 0)){
        txt += '<h2>Aliases</h2><ul>';
        txt += itemizeArray(json.alias) + '</ul>';
    }
    if((json.actor != null) && (json.actor.length > 0)){
        txt += '<h2>Actors Who Played This Character</h2><ul>'
            for(i = 0; i < json.actor.length; i++){
                txt += '<li><a href="' + writeURL(json.actor[i],3) + '">' +
                    json.actor[i] + '</a></li>';
            }
        txt += '</ul>';
    }
    if((json.media != null) && (json.media.length > 0)){
        txt += '<h2>Appearances in Media</h2><ul>';
        for(i = 0; i < json.media.length; i++){
            txt += '<li><a href="' + writeURL(json.media[i],2) + '">' +
                json.media[i] + '</a></li>';
        }
        txt += '</ul>';
    }
    return txt;
}

function writeURL(c,p){
    return '/' + p + '/' + c.replace(/ /g,'_') + '.html';
}

function writeMediaPage(){
    var txt = '';
    if((json.chars !== null) && (json.chars.length > 0)){
        txt += '<h2>Characters and Actors</h2><ul>';
        for(var i = 0; i < json.chars.length; i++){
            txt += '<li><a href="' + writeURL(json.chars[i].chr,1) + 
                '">' + json.chars[i].chr + '</a> - <a href="' + 
                writeURL(json.chars[i].act,3) + '">' + json.chars[i].act +
                '</a></li>';
        }
        txt += '</ul>';
    }
    return txt;
}

function writeActorPage(){
    var txt = '';
    if((json.chars !== null) && (json.chars.length > 0)){
        txt += '<h2>Characters Played</h2>';
        for(var i = 0; i < json.chars.length; i++){
            txt += '<li><a href="' + writeURL(json.chars[i],1) +
                '">' + json.chars[i] + '</a></li>';
        }
        txt += '</ul>';
    }
    if((json.media !== null) && (json.media.length > 0)){
        txt += '<h2>Appearances in Media</h2>';
        for(var i = 0; i < json.media.length; i++){
            txt += '<li><a href="' + writeURL(json.media[i],2) + '">' +
                json.media[i] + '</a></li>'; 
        }
        txt += '</ul>';
    }
    return txt;
}

function writePage(){
    var content = document.getElementById('main-content');
    var text = makePageHeader();
    text += '\n<div id="desc"></div>\n';
    text += '<a id="flip" href="javascript:showEditor()">Edit Description</a>';
    switch(params.p){
        case '1':
            text += writeCharacterPage();
            break;
        case '2':
            text += writeMediaPage();
            break;
        case '3':
            text += writeActorPage();
            break;
    }
    // All Done
    content.innerHTML = text;
    handleDescript();
}

/* Writes the pages subject description/article 
*/
function handleDescript(){
    var converter = new showdown.Converter();
    document.getElementById('desc').innerHTML = converter.makeHtml(json.desc);
}

/* This Function is called on page load.
 * It sets up the page and makes the requests neccessary to display the page
 */
function onPageLoad(){
    params = getSearchParameters();
    var url = '/dat/' + params.c.replace(/\./g, '') + '.json';
    makeJSONRequest(url);
    setTimeout(writePage, 275);
}

/* Replaces the character desc with a form containing text area and a submit
 * button that updates the page information.
 */
function showEditor(){
    document.getElementById('desc').innerHTML = 
        '<form id="ud" action="javascript:submitDescUpdate()">' +
        '<textarea name="d" rows=8 cols=80>' + json.desc + 
        '</textarea><input type="submit" value="submit"></form>';
    document.getElementById('flip').href = 'javascript:writePage()';
    document.getElementById('flip').innerHTML = "Return to Article";
}

function submitDescUpdate(){
    var http = new XMLHttpRequest();
    var url = "/editpage.php";
    var p = 'd=' + document.forms['ud']['d'].value + '&p=' + params.p +
       '&n=' + json.name;

    if(p === json.desc){
        console.log("A submit is not required");
        return;
    }
    http.open("POST", url, true);

    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.setRequestHeader("Content-length", params.length);
    http.setRequestHeader("Connection", "close");

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            console.log(http.responseText);
        }
    }
    http.send(p);

    // reload the page data in 150ms, because the server will have updated 
    // the page data by then.
    setTimeout(onPageLoad,150);
}
