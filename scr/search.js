/* scr/search.js
 * The search form verification script
 * Henry J Schmale
 * July 20, 2015
 */

function srchk(form){
    if(form.media.value != "0"){
        return true;
    }
    if(form.q.value.length == 0){
        alert("No Blank Search Queries Are Allowed");
        form.q.focus();
        return false;
    }
    return true;
}
