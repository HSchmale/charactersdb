/* searchres.js
 * The search results rewrite script, rewrites certain table elements in the
 * results to save on server side bandwidth, and keep server code clean.
 * Henry J Schmale
 * July 20, 2015
 */

/* makes a param valid for param c in the infopage get request
 */
function writeURL(c,p){
     return '/' + p + '/' + c.replace(/ /g,'_') + '.html';
}

function rewriteTable(){
    var elem = document.querySelectorAll(".char");
    var i;
    var td,td2;
    console.log(elem.length);
    for(i = 0; i < elem.length; i++){
        td = elem[i].innerHTML;
        td = '<a href="' + writeURL(td,1) + '">' + td + '</a>';
        elem[i].innerHTML = td;
    }
    elem = document.querySelectorAll(".series");
    for(i = 0; i < elem.length; i++){
        td = elem[i].innerHTML;
        td = '<a href="' + writeURL(td,2) + '">' + td + '</a>';
        elem[i].innerHTML = td;
    }
    elem = document.querySelectorAll(".actor");
    for(i = 0; i < elem.length; i++){
        td = elem[i].innerHTML;
        td = '<a href="' + writeURL(td,3) + '">' + td + '</a>';
        elem[i].innerHTML = td;
    }
}
