<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>Search: Characters Database</title>
<meta name="description"
     content="A database of the appearences of various characters in fiction"/>
<link rel="stylesheet" href="/sty/main.css" media="screen" />
<link rel="shortcut icon" href="/img/icon.png" />
<script src="scr/sorttable.js" async></script>
<script src="scr/searchres.js"></script>
<script>
    window.onload = rewriteTable;
</script>
</head>

<body>
<div id="page-container">
<?php
include('inc/header.html');
include('inc/db.php');
include('inc/const.php');
// code to add query stats
$sql='SELECT ADDQUERYSTAT(\''.$_GET['q'].'\',\''.$_GET['search_t'].'\','.$_GET['media'].');';
$t = $dbh->exec($sql);

function chrSearchByName(){
    $sql='SELECT * FROM VW_CHRAPPS WHERE UPPER(ALIAS) LIKE UPPER(:q)';
    // Media Handling
    if($_GET['media'] != 0){
        // The Media_t is not any
        $sql .= ' AND MID='.$_GET['media'];
    }
    $sql .=';';
    return $sql;
}

function actSearchByRole(){
    return 'SELECT * FROM VW_ACTAPPS WHERE UPPER(ACTOR) LIKE UPPER(:q);';
} 
?>

<div id="main-content">
<?php include("inc/ads.html"); ?>
<h1><?php echo $SEARCH_T[$_GET['search_t']]; ?> Appearance Search Results</h1>
<div id="res-count">
<?php
// prep the search query
$_GET['q'] = '%'.trim($_GET['q']).'%';
switch($_GET['search_t']){
case 'cn': // character by name
    $sql = chrSearchByName();
    break;
case 'ar': // actor by role
    $sql = actSearchByRole();
    break;
}
$appstmt=$dbh->prepare($sql);
$appstmt->bindParam(':q',$_GET['q'],PDO::PARAM_STR);
$appstmt->execute();
echo '<p>',$appstmt->rowCount(),' Results Returned For "',$_GET['q'],'" In ',
    $MEDIA_TYPES[(int)($_GET['media'])],'</p>',
    '</div><!-- res-count -->';
?>
<table class="sortable">
<?php
switch($_GET['search_t']){
case 'cn': // character by name
    echo '<tr><th>Character<br/>Name</th>',
        '<th>Media<br/>Type</th>',
        '<th>Series Name</th>',
        '<th>Episode</th>',
        '<th>Episode Id</th>',
        '<th>Notes</th></tr>';
    foreach($appstmt as $r){
        $epid = $r['season'].'x'.$r['episid'];
        echo '<tr>',
         '<td class="char">',$r['char_name'],'</td>',
         '<td>',$r['media'],'</td>',
         '<td class="series">',$r['series_name'],'</td>',
         '<td>',$r['episode'],'</td>',
         '<td>',$epid,'</td>',
         '<td>',$r['notes'],'</td></tr>';
    }
    break;
case 'ar': // Actor by role name
    echo '<tr><th>Character</th>',
         '<th>Actor</th>',
         '<th>Series<br/>Name</th></tr>';
    foreach($appstmt as $r){
        echo '<tr>',
        '<td class="char">',$r['chract'],'</td>',
        '<td class="actor">',$r['actor'],'</td>',
        '<td class="series">',$r['series_name'],'</td></tr>';
    }
    break;
}
?></tbody>
</table>
<h1>Search Again</h1>
<?php include_once('inc/searchform.html') ?>

</div><!-- main-content -->

<?php include_once('inc/footer.html'); ?>
</div><!-- page-container -->
</body>
</html>
