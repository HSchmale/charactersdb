-- The database setup script for the characters database
-- This is for a PostgreSQL database
-- Henry J Schmale
-- July 13, 2015

------------------------------------------------------------
-- Tables to assist with listing options and variables for queries

-- Script Variables
\set UNKNOWN '\'No Information Available\''

-- Media Types Table
CREATE TABLE MEDIA_T(
  TYP           VARCHAR(12) PRIMARY KEY NOT NULL,
  SEQ           SERIAL UNIQUE
);
INSERT INTO MEDIA_T(TYP) VALUES
  ('BOOK'),('MOVIE'),('TV SHOW'),('VIDEO GAME'),('COMICS');

-----------------------------------------------------------
-- Data Storage Tables

-- Table of fictional universes
CREATE TABLE UNIVERSES(
  ID            SERIAL PRIMARY KEY NOT NULL,
  NAME          VARCHAR(64) UNIQUE,
  DESCR         TEXT DEFAULT :UNKNOWN
);
CREATE UNIQUE INDEX UX_UNIVERS_NAME ON UNIVERSES(LOWER(NAME));

-- Table of various differint medias.
-- books, movies, and tv shows.
CREATE TABLE MEDIAS(
  ID            SERIAL PRIMARY KEY NOT NULL,
  NAME          VARCHAR(255),           -- Media Title
  WIKILINK      TEXT,                   -- a link to the wikipedia page
  MEDIA         INTEGER REFERENCES MEDIA_T(SEQ), -- the media type
  YEARAIRED     INTEGER DEFAULT 0,      -- year first released, published, aired
  UNIVID        INTEGER DEFAULT 1,      -- universe series takes place in, default no universe
  SERIES        BOOLEAN DEFAULT FALSE,  -- is a series
  DESCR         TEXT DEFAULT :UNKNOWN   -- brief intro of the media
);
CREATE UNIQUE INDEX UX_MEDIA_IT ON MEDIAS(NAME,YEARAIRED,MEDIA,UNIVID);
CREATE INDEX IX_MEDIA_NAME ON MEDIAS(NAME);

-- Table of the characters
CREATE TABLE CHARACTERS(
  ID            SERIAL PRIMARY KEY NOT NULL,
  UNIVID        INTEGER,               -- id of the universe in appears
  NAME          VARCHAR(128),          -- real character name
  DESCR         TEXT DEFAULT :UNKNOWN  -- a description of the character
);
CREATE UNIQUE INDEX UX_CHAR_NAME ON CHARACTERS(UPPER(NAME));
CREATE INDEX IX_CHARACTERS_NAME ON CHARACTERS(NAME);

-- Table of episodes of all medias
-- For series the season and episode column are set to the season and number
-- of episode being referenced for TV SHOWS. However for all other media they
-- are set to 0.
CREATE TABLE EPISODES(
  ID            SERIAL PRIMARY KEY NOT NULL,
  SEASON        INTEGER DEFAULT 0,  -- show season number aired
  EPISODE       INTEGER DEFAULT 0,  -- number aired within that season
  MEDIAID       INTEGER,            -- id of media
  NAME          VARCHAR(128),       -- episode name
  AIRDATE       DATE,               -- date aired
  SYNOPSIS      TEXT
);
CREATE UNIQUE INDEX UX_EPI_NAME ON EPISODES(NAME,MEDIAID);
CREATE INDEX EPI_NAME ON EPISODES(NAME);

-- Table of actors and what character they played
CREATE TABLE ACTORS(
  ID            SERIAL PRIMARY KEY NOT NULL,
  NAME          VARCHAR(128),       -- actor name
  WIKILINK      VARCHAR(384),       -- link to wikipedia article
  DESCR         TEXT DEFAULT :UNKNOWN
);
CREATE UNIQUE INDEX UX_ACTOR_NAME ON ACTORS(UPPER(NAME));
CREATE INDEX IX_ACTOR_NAME ON ACTORS(NAME);

-- Acto Appearences Table, listing of appearances of various characters
CREATE TABLE ACTAPPS(
  ID            SERIAL PRIMARY KEY NOT NULL,
  ACTID         INTEGER REFERENCES ACTORS(ID),
  CHRID         INTEGER REFERENCES CHARACTERS(ID),
  MEDIAID       INTEGER REFERENCES MEDIAS(ID)
);
--CREATE UNIQUE INDEX UX_ACCAPP ON ACTAPPS(ACTID,CHRID,MEDIAID);

-- Table of apperences of various characters by episode and media id
CREATE TABLE CHRAPPS(
  ID            SERIAL PRIMARY KEY NOT NULL,
  CHARID        INTEGER REFERENCES CHARACTERS(ID),  -- id of character
  MEDIAID       INTEGER REFERENCES MEDIAS(ID),      -- id of media
  EPISODEID     INTEGER REFERENCES EPISODES(ID),    -- id of episode character appears in
  NOTES         TEXT                                -- errata about appearence
);
CREATE UNIQUE INDEX UX_APPS ON CHRAPPS(CHARID,MEDIAID,EPISODEID);

-- Aliases of various characters 
-- Superheros have secrect ids list them here
CREATE TABLE CHRALIASES(
  ID            SERIAL PRIMARY KEY NOT NULL,
  CHARID        INTEGER,            -- character id being aliased
  NAME          VARCHAR(128)        -- alternate name
);
CREATE UNIQUE INDEX UX_CHRALIAS_NAME ON CHRALIASES(NAME,CHARID);

-----------------------------------------------------------
-- Views for searching databases
CREATE VIEW VW_CHRALIASES AS
SELECT
  C.NAME || ' ' || COALESCE(STRING_AGG(A.NAME,' '), '') AS ALIAS,
  C.NAME AS NAME,
  C.ID AS ID
FROM CHARACTERS C
LEFT JOIN CHRALIASES A ON C.ID=A.CHARID
GROUP BY C.ID;

CREATE VIEW VW_CHRAPPS AS
SELECT 
  A.ID      AS ID,
  C.ID      AS CHRID,
  C.NAME    AS CHAR_NAME,
  C.ALIAS   AS ALIAS,
  MT.TYP    AS MEDIA,   -- Media Type Name
  MT.SEQ    AS MID,     -- Media Type Id
  M.NAME    AS SERIES_NAME,
  E.NAME    AS EPISODE,
  E.SEASON  AS SEASON,
  E.EPISODE AS EPISID,
  A.NOTES   AS NOTES
FROM CHRAPPS A
LEFT JOIN EPISODES E        ON E.ID=A.EPISODEID
LEFT JOIN MEDIAS M          ON M.ID=A.MEDIAID
LEFT JOIN VW_CHRALIASES C   ON C.ID=A.CHARID
LEFT JOIN MEDIA_T MT        ON MT.SEQ=M.MEDIA;

CREATE VIEW VW_ACTAPPS AS
SELECT 
  actapps.id AS appid, 
  medias.name AS series_name,
  characters.id AS chrid,
  characters.name AS chract, 
  actors.name AS actor,
  actors.id   AS actid
FROM 
  public.actapps, 
  public.characters, 
  public.actors, 
  public.medias
WHERE 
  actapps.actid = actors.id AND
  actapps.chrid = characters.id AND
  actapps.mediaid = medias.id;

CREATE VIEW VW_CHRMEDIA AS 
SELECT DISTINCT
  characters.name AS name, 
  chrapps.mediaid AS mediaid
FROM 
  public.chrapps, 
  public.characters
WHERE 
  chrapps.charid = characters.id;

CREATE VIEW VW_ACT_SIMP AS
SELECT 
  actors.name AS act_name, 
  characters.name AS chr_name, 
  actapps.mediaid AS mid
FROM 
  public.actors, 
  public.characters, 
  public.actapps
WHERE 
  actors.id = actapps.actid AND
  characters.id = actapps.chrid;

-----------------------------------------------------------
-- Database Statistics Stuff
-- Things to record how the database is used

-- Record how the database grows
CREATE TABLE RECORDSZ(
  ID            SERIAL PRIMARY KEY NOT NULL,
  DAYREC        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  APPSNUM       INTEGER DEFAULT 0,
  CHARNUM       INTEGER DEFAULT 0,
  MEDINUM       INTEGER DEFAULT 0,
  UNIVNUM       INTEGER DEFAULT 0,
  EPISNUM       INTEGER DEFAULT 0,
  ACTONUM       INTEGER DEFAULT 0
);

CREATE TABLE QUERYSTATS(
  ID            SERIAL PRIMARY KEY NOT NULL,
  Q             VARCHAR(64),                          -- Query
  ST            VARCHAR(4),                           -- Search Type
  MT            INTEGER,                              -- Media Type
  DAYREC        TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE FUNCTION ADDQUERYSTAT(Q VARCHAR(64), ST VARCHAR(4), MT INTEGER)
  RETURNS INTEGER AS $$
  BEGIN
    INSERT INTO QUERYSTATS(Q,MT,ST) VALUES(Q, MT, ST);
    RETURN 0;
  END
  $$ LANGUAGE PLPGSQL;

CREATE FUNCTION UPDATERECORDSZ() RETURNS TRIGGER AS $$
  DECLARE
    APPSN  INTEGER;
    CHARN  INTEGER;
    MEDIAN INTEGER;
    UNIN   INTEGER;
    EPIN   INTEGER;
    ACTRN  INTEGER;
  BEGIN
    -- init vars
    SELECT COUNT(*) INTO APPSN FROM CHRAPPS;
    SELECT COUNT(*)+APPSN INTO APPSN FROM ACTAPPS;
    SELECT COUNT(*) INTO CHARN FROM CHARACTERS;
    SELECT COUNT(*) INTO MEDIAN FROM MEDIAS;
    SELECT COUNT(*) INTO UNIN FROM UNIVERSES;
    SELECT COUNT(*) INTO EPIN FROM EPISODES;
    SELECT COUNT(*) INTO ACTRN FROM ACTORS;

    INSERT INTO RECORDSZ( APPSNUM, CHARNUM, MEDINUM, UNIVNUM,
                          EPISNUM, ACTONUM)
      VALUES(APPSN, CHARN, MEDIAN, UNIN, EPIN, ACTRN);
    RETURN NEW;
  END
  $$ LANGUAGE PLPGSQL;

-- triggers for updating recordsz
CREATE TRIGGER APPS_INSERT
  AFTER INSERT ON CHRAPPS
  EXECUTE PROCEDURE UPDATERECORDSZ();
CREATE TRIGGER CHAR_INSERT
  AFTER INSERT ON CHARACTERS
  EXECUTE PROCEDURE UPDATERECORDSZ();
CREATE TRIGGER MEDIA_INSERT
  AFTER INSERT ON MEDIAS
  EXECUTE PROCEDURE UPDATERECORDSZ();
CREATE TRIGGER UNI_INSERT
  AFTER INSERT ON UNIVERSES
  EXECUTE PROCEDURE UPDATERECORDSZ();
CREATE TRIGGER EPI_INSERT
  AFTER INSERT ON EPISODES
  EXECUTE PROCEDURE UPDATERECORDSZ();

-----------------------------------------------------------
-- Database Audit Stuff

CREATE schema audit;
REVOKE CREATE ON schema audit FROM public;
 
CREATE TABLE audit.logged_actions (
  id            SERIAL PRIMARY KEY NOT NULL,
  schema_name   text NOT NULL,
  TABLE_NAME    text NOT NULL,
  user_name     text,
  action_tstamp TIMESTAMP WITH TIME zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
  action        TEXT NOT NULL CHECK (action IN ('I','D','U')),
  original_data text,
  new_data      text,
  query         text
) WITH (fillfactor=100);
 
REVOKE ALL ON audit.logged_actions FROM public;
 
-- You may wish to use different permissions; this lets anybody
-- see the full audit data. In Pg 9.0 and above you can use column
-- permissions for fine-grained control.
GRANT SELECT ON audit.logged_actions TO public;
 
CREATE INDEX logged_actions_schema_table_idx 
ON audit.logged_actions(((schema_name||'.'||TABLE_NAME)::TEXT));
 
CREATE INDEX logged_actions_action_tstamp_idx 
ON audit.logged_actions(action_tstamp);
 
CREATE INDEX logged_actions_action_idx 
ON audit.logged_actions(action);
 
--
-- Now, define the actual trigger function:
--
CREATE OR REPLACE FUNCTION audit.if_modified_func() RETURNS TRIGGER AS $body$
DECLARE
  v_old_data TEXT;
  v_new_data TEXT;
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    v_old_data := ROW(OLD.*);
    v_new_data := ROW(NEW.*);
    INSERT INTO audit.logged_actions (schema_name,table_name,user_name,action,original_data,new_data,query) 
    VALUES (TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data,v_new_data, current_query());
    RETURN NEW;
  ELSIF (TG_OP = 'DELETE') THEN
    v_old_data := ROW(OLD.*);
    INSERT INTO audit.logged_actions (schema_name,table_name,user_name,action,original_data,query)
    VALUES (TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data, current_query());
    RETURN OLD;
  ELSIF (TG_OP = 'INSERT') THEN
    v_new_data := ROW(NEW.*);
    INSERT INTO audit.logged_actions (schema_name,table_name,user_name,action,new_data,query)
    VALUES (TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_new_data, current_query());
    RETURN NEW;
  ELSE
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - Other action occurred: %, at %',TG_OP,now();
    RETURN NULL;
  END IF;

EXCEPTION
  WHEN data_exception THEN
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
    RETURN NULL;
  WHEN unique_violation THEN
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
    RETURN NULL;
  WHEN OTHERS THEN
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
    RETURN NULL;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = pg_catalog, audit;
 
--
-- To add this trigger to a table, use:
-- CREATE TRIGGER tablename_audit
-- AFTER INSERT OR UPDATE OR DELETE ON tablename
-- FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
--

CREATE TRIGGER MEDIAS_audit
AFTER UPDATE OR DELETE ON MEDIAS
FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();

CREATE TRIGGER CHARACTERS_audit
AFTER UPDATE OR DELETE ON CHARACTERS
FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();

CREATE TRIGGER ACTORS_audit
AFTER UPDATE OR DELETE ON ACTORS
FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
